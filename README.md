# Billetera digital

Este es un desarrollo de prueba para ePayco

### Requisitos

Para poder ejecutar el proyecto debe tener instalado los siguientes componentes:

```
Composer
MySql Server
Linux
PHP > 7.1.3
Apache server
Yarn o npm
```

### Configuración del proyecto


Paso 1

```
git clone https://gitlab.com/felipemesa/test-epayco-wallet.git
```


Paso 2

```
cd /test-epayco-wallet
```

Paso 3

```
composer install
```

Paso 4

```
yarn install
```

Paso 5

En la raiz del proyecto modificar en el archivo .env la linea que se describe a continuación con sus datos de conexión a su servidor MySql y el nombre de la base de datos. 

```
DATABASE_URL=mysql://usuario:contraseña@127.0.0.1:3306/baseDeDatos
```

Modifica el .env y agregar la configuración de conexión a correo electronico Ejm

```
###> symfony/mailer ###
MAILER_DSN=smtp://info@prontofixgas.com:Papelon_1217@smtp-es.securemail.pro:587
###< symfony/mailer ###
```

Modifica el .env y agregar las rutas de los 2 servidores Api con app y Soap Ejm

```
BASE_URL=http://127.0.0.1:8000
BASE_URL_SOAP=http://127.0.0.1:8001
```


Paso 7

```
php bin/console doctrine:database:create
```

Paso 8

```
php bin/console doctrine:schema:update --force
```

paso 9
Ejecutar la api y la app con los siguientes comandos
```
php bin/console doctrine:schema:update --force
```

Ejecutar los siguientes comandos para correr la aplicación en diferentes servidores
```
php -S localhost:8000 -t public
yarn run dev-server o yarn ru encore dev --watch

Estepara correr el soap dependiente de la url definida en el .env
php -S localhost:8001 -t public
```

Adjunto documentación de los endpoints documentados en postmant.
```
https://documenter.getpostman.com/view/6859942/SztD78XX?version=latest
```

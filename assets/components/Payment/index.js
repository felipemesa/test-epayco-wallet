import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {makeStyles} from '@material-ui/core/styles';
import Title from "../Title";
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogContentText from '@material-ui/core/DialogContentText';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));


export default function Payment() {
    const [loading, setLoading] = useState(false);
    const [submit, setsubmit] = useState(false);
    const [submitDialog, setSubmitDialog] = useState(false);
    const [error, setError] = useState("");
    const [errorDialog, setErrorDialog] = useState("");
    const [success, setSuccess] = useState("");
    const [dialog, setDialog] = useState(false);
    const [dialog2, setDialog2] = useState(false);

    const [user, setUser] = useState({
        name: "",
        identificationNumber: "",
        mobile: "",
        email: "",
        balance: "",
        token: "",
    });

    const [state, setState] = useState({
        identificationNumber: "",
        email: "",
        total: "",
    });

    let refresh = x => setState(Object.assign({}, state, x));


    const hideError = () => {
        setTimeout(() => {
            setError("")
        }, 5000)
    };

    const request = () => {
        setsubmit(true);
        if (state.identificationNumber && state.email && state.total && state.total > 0) {
            setLoading(true);
            axios.post(`/api/payment/pay`, {
                identificationNumber: state.identificationNumber,
                email: state.email,
                total: state.total,

            }).then(response => {
                const {data} = response;
                if (data.success) {
                    setSuccess(data.message);
                    setUser(data.data);
                    setDialog(true);
                } else {
                    setError(data.message)
                }
                setLoading(false);
            }).catch(error => {
                setError(error.message);
                setLoading(false);
            })
        } else {
            setError("Por favor corregir los campos obligatorios")
        }
        hideError();
    };

    const confirmPayment = () => {
        setSubmitDialog(true);
        if (state.identificationNumber && state.token) {
            setLoading(true);
            axios.post(`/api/payment/confirm`, {
                identificationNumber: state.identificationNumber,
                token: state.token,
            }).then(response => {
                const {data} = response;
                if (data.success) {
                    setSuccess(data.message);
                    setUser(data.data);
                    setDialog(false);
                    setDialog2(true);
                } else {
                    setErrorDialog(data.message)
                }
                setLoading(false);
            }).catch(error => {
                setErrorDialog(error.message);
                setLoading(false);
            })
        } else {
            setErrorDialog("Por favor corregir los campos obligatorios")
        }
        hideError();
    };

    const classes = useStyles();
    return (
        <React.Fragment>
            {error && <Alert severity="error">{error}</Alert>}
            {success && <Alert severity="success">{success}</Alert>}
            <Title>Realizar Pago</Title>
            <form className={classes.root} autoComplete="off" onSubmit={() => request()}>
                <div>
                    <TextField
                        id="outlined-password-input"
                        label="Documento"
                        name={"identificationNumber"}
                        type="text"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.identificationNumber}
                        helperText={submit && !state.identificationNumber ? "Por favor ingresar un documento" : ""}
                    />
                    <TextField
                        minLength={10}
                        maxLength={10}
                        id="outlined-password-input"
                        label="Correo"
                        name={"email"}
                        type="email"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.email}
                        helperText={submit && !state.email ? "Por favor ingresar un email válido" : ""}
                    />
                    <TextField
                        minLength={10}
                        maxLength={10}
                        id="outlined-password-input"
                        label="Total a recargar"
                        name={"total"}
                        type="number"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.total && state.total <= 0}
                        helperText={submit && !state.total && state.total <= 0 ? "Por favor ingresar un valor válido" : ""}
                    />
                </div>
                <div>
                    <Button variant="contained" color="primary" size="large" onClick={request}>
                        {loading ?
                            <CircularProgress color="secondary"/> :
                            "Pagar"
                        }
                    </Button>
                </div>
            </form>
            <br/>

            <div>
                <Dialog open={dialog} onClose={() => setDialog(false)} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Confirmar pago</DialogTitle>
                    <DialogContent>
                        {errorDialog && <Alert severity="error">{errorDialog}</Alert>}
                        <DialogContentText>
                            Hemos enviado un correo electrónico con un token, por favor confirmarlo en este formulario
                            para proceder el pago..
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="token"
                            name="token"
                            label="Token"
                            type="text"
                            fullWidth
                            onChange={(e) => refresh({[e.target.name]: e.target.value})}
                            error={submitDialog && !state.token}
                            helperText={submitDialog && !state.token ? "Por favor ingresar un token válido" : ""}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setDialog(false)}>
                            Cancel
                        </Button>
                        <Button onClick={() => confirmPayment()} color="primary">
                            {loading ?
                                <CircularProgress color="secondary"/> :
                                "Pagar"
                            }
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
            <div>
                <Dialog open={dialog2} onClose={() => setDialog2(false)} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Pago confirmado:</DialogTitle>
                    <DialogContent>
                        {success && <Alert severity="success">{success}</Alert>}
                        <Typography color="textSecondary" className={classes.depositContext}>
                            Su nuevo saldo es de:
                        </Typography>
                        <Typography component="p" variant="h4">
                            ${user.balance}
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setDialog2(false)} color="primary">
                            Terminar
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        </React.Fragment>
    );
}
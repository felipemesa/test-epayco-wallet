import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {makeStyles} from '@material-ui/core/styles';
import Title from "../Title";
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));


export default function User() {
    const [loading, setLoading] = useState(false);
    const [submit, setsubmit] = useState(false);
    const [error, setError] = useState("");
    const [success, setSuccess] = useState("");

    const [user, setUser] = useState({
        name: "",
        identificationNumber: "",
        mobile: "",
        email: "",
    });

    const [state, setState] = useState({
        name: "",
        identificationNumber: "",
        mobile: "",
        email: "",
    });

    let refresh = x => setState(Object.assign({}, state, x));


    const hideError = () => {
        setTimeout(() => {
            setError("")
        }, 5000)
    };

    const request = () => {
        setsubmit(true);
        if (state.name && state.identificationNumber && state.mobile && state.email) {
            if (state.mobile.length === 10) {
                setLoading(true);
                axios.post(`/api/create/user`, {
                    name: state.name,
                    identificationNumber: state.identificationNumber,
                    mobile: state.mobile,
                    email: state.email,

                }).then(response => {
                    const {data} = response;
                    if (data.success) {
                        setSuccess(data.message);
                        setUser(data.data);
                    } else {
                        setError(data.message)
                    }
                    setLoading(false);
                }).catch(error => {
                    setError(error.message);
                    setLoading(false);
                })
            } else {
                setError("El campo celular debe ser de 10 digitos")
            }
        } else {
            setError("Por favor corregir los campos obligatorios")
        }
        hideError();
    };

    const classes = useStyles();
    return (
        <React.Fragment>
            {error && <Alert severity="error">{error}</Alert>}
            {success && <Alert severity="success">{success}</Alert>}
            <Title>Crear usuario</Title>
            <form className={classes.root} autoComplete="off" onSubmit={() => request()}>
                <div>
                    <TextField
                        id="outlined-password-input"
                        label="Nombre"
                        type="text"
                        name={"name"}
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.name}
                        helperText={submit && !state.name ? "Por favor ingresar un nombre" : ""}
                    />
                    <TextField
                        id="outlined-password-input"
                        label="Documento"
                        name={"identificationNumber"}
                        type="text"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.identificationNumber}
                        helperText={submit && !state.identificationNumber ? "Por favor ingresar un documento" : ""}
                    />
                    <TextField
                        minLength={10}
                        maxLength={10}
                        id="outlined-password-input"
                        label="Celular"
                        name={"mobile"}
                        type="text"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.mobile}
                        helperText={submit && !state.mobile ? "Por favor ingresar un celular" : ""}
                    />
                    <TextField
                        id="outlined-password-input"
                        label="Correo"
                        name={"email"}
                        type="text"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        validators={['required', 'isEmail']}
                        error={submit && !state.email}
                        helperText={submit && !state.email ? "Por favor ingresar un correo" : ""}
                    />
                </div>
                <div>
                    <Button variant="contained" color="primary" size="large" onClick={request}>
                        {loading ?
                            <CircularProgress color="secondary"/> :
                            "Guardar"
                        }
                    </Button>
                </div>
            </form>
            <br/>
            <div>
                {user.name &&
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Nombre</TableCell>
                            <TableCell>Documento</TableCell>
                            <TableCell>Celular</TableCell>
                            <TableCell>Email</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>{user.name}</TableCell>
                            <TableCell>{user.identificationNumber}</TableCell>
                            <TableCell>{user.mobile}</TableCell>
                            <TableCell>{user.email}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                }
            </div>
        </React.Fragment>
    );
}
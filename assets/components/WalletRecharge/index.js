import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {makeStyles} from '@material-ui/core/styles';
import Title from "../Title";
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContentText from '@material-ui/core/DialogContentText';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));


export default function WalletRecharge() {
    const [loading, setLoading] = useState(false);
    const [submit, setsubmit] = useState(false);
    const [error, setError] = useState("");
    const [success, setSuccess] = useState("");
    const [dialog, setDialog] = useState(false);

    const [user, setUser] = useState({
        name: "",
        identificationNumber: "",
        mobile: "",
        email: "",
        balance: "",
    });

    const [state, setState] = useState({
        identificationNumber: "",
        mobile: "",
        total: "",
    });

    let refresh = x => setState(Object.assign({}, state, x));


    const hideError = () => {
        setTimeout(() => {
            setError("")
        }, 5000)
    };

    const request = () => {
        setsubmit(true);
        if (state.identificationNumber && state.mobile && state.total && state.total > 0) {
            if (state.mobile.length === 10) {
                setLoading(true);
                axios.post(`/api/recharge/wallet`, {
                    identificationNumber: state.identificationNumber,
                    mobile: state.mobile,
                    total: state.total,

                }).then(response => {
                    const {data} = response;
                    if (data.success) {
                        setSuccess(data.message);
                        setUser(data.data);
                        setDialog(true);
                    } else {
                        setError(data.message)
                    }
                    setLoading(false);
                }).catch(error => {
                    setError(error.message);
                    setLoading(false);
                })
            } else {
                setError("El campo celular debe ser de 10 digitos")
            }
        } else {
            setError("Por favor corregir los campos obligatorios")
        }
        hideError();
    };

    const classes = useStyles();
    return (
        <React.Fragment>
            {error && <Alert severity="error">{error}</Alert>}
            {success && <Alert severity="success">{success}</Alert>}
            <Title>Recargar Saldo</Title>
            <form className={classes.root} autoComplete="off" onSubmit={() => request()}>
                <div>
                    <TextField
                        id="outlined-password-input"
                        label="Documento"
                        name={"identificationNumber"}
                        type="text"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.identificationNumber}
                        helperText={submit && !state.identificationNumber ? "Por favor ingresar un documento" : ""}
                    />
                    <TextField
                        minLength={10}
                        maxLength={10}
                        id="outlined-password-input"
                        label="Celular"
                        name={"mobile"}
                        type="text"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.mobile}
                        helperText={submit && !state.mobile ? "Por favor ingresar un celular" : ""}
                    />
                    <TextField
                        minLength={10}
                        maxLength={10}
                        id="outlined-password-input"
                        label="Total a recargar"
                        name={"total"}
                        type="number"
                        variant="outlined"
                        required={true}
                        onChange={(e) => refresh({[e.target.name]: e.target.value})}
                        error={submit && !state.total && state.total <= 0}
                        helperText={submit && !state.total && state.total <= 0 ? "Por favor ingresar un valor válido" : ""}
                    />
                </div>
                <div>
                    <Button variant="contained" color="primary" size="large" onClick={request}>
                        {loading ?
                            <CircularProgress color="secondary"/> :
                            "Guardar"
                        }
                    </Button>
                </div>
            </form>
            <br/>
            <div>
                <Dialog onClose={() => setDialog(false)} aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description" open={dialog}>
                    <DialogTitle id="alert-dialog-titl">Recarga exitosa</DialogTitle>
                    <DialogContentText id="alert-dialog-description">
                        <Typography color="textSecondary" className={classes.depositContext}>
                            Su nuevo saldo es de:
                        </Typography>
                        <Typography component="p" variant="h4">
                            ${user.balance}
                        </Typography>
                    </DialogContentText>
                </Dialog>
            </div>
        </React.Fragment>
    );
}
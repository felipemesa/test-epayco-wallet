<?php

namespace App\Controller\Api;

use nusoap_client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\Mailer\MailerInterface;

class ApiPaymentController extends AbstractController
{

    /**
     * @Rest\Post("/api/payment/pay")
     */
    public function pay(Request $request, MailerInterface $mailer)
    {
        $client = new nusoap_client(getenv("BASE_URL_SOAP") . '/payment/soap/pay?wsdl', 'wsdl');
        $client->setEndpoint(getenv("BASE_URL_SOAP") . '/payment/soap/pay');

        $client->decode_utf8 = true;

        $objData = json_decode($request->getContent());

        $email = isset($objData->email) ? $objData->email : "";
        $identificationNumber = isset($objData->identificationNumber) ? $objData->identificationNumber : "";
        $total = isset($objData->total) ? $objData->total : "";

        // Calls
        $result = $client->call('pay', ['identificationNumber' => $identificationNumber, 'email' => $email, 'total' => $total]);

        $response = json_decode($result);

        return new JsonResponse($response);
    }


    /**
     * @Rest\Post("/api/payment/confirm")
     */
    public function paymentConfirm(Request $request)
    {
        $client = new nusoap_client(getenv("BASE_URL_SOAP") . '/payment/soap/confirm?wsdl', 'wsdl');
        $client->setEndpoint(getenv("BASE_URL_SOAP") . '/payment/soap/confirm');

        $client->decode_utf8 = true;

        $objData = json_decode($request->getContent());

        $token = isset($objData->token) ? $objData->token : "";
        $identificationNumber = isset($objData->identificationNumber) ? $objData->identificationNumber : "";

        // Calls
        $result = $client->call('paymentConfirm', ['token' => $token, 'identificationNumber' => $identificationNumber]);

        $response = json_decode($result);


        return new JsonResponse($response);
    }

}
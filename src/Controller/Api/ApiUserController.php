<?php

namespace App\Controller\Api;

use nusoap_client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

class ApiUserController extends AbstractController
{
    /**
     * @Rest\Post("/api/create/user")
     * @param Request $request
     * @return JsonResponse
     */
    public function createUser(Request $request)
    {
        $client = new nusoap_client(getenv("BASE_URL_SOAP").'/create/user/soap?wsdl', 'wsdl');
        $client->setEndpoint(getenv("BASE_URL_SOAP").'/create/user/soap');

        $client->decode_utf8 = true;

        $objData = json_decode($request->getContent());

        $name = isset($objData->name) ? $objData->name : "";
        $mobile = isset($objData->mobile) ? $objData->mobile : "";
        $email = isset($objData->email) ? $objData->email : "";
        $identificationNumber = isset($objData->identificationNumber) ? $objData->identificationNumber : "";

        $result = $client->call('createUser', ['name' => $name, 'identificationNumber' => $identificationNumber, 'mobile' => $mobile, 'email' => $email]);

        $arrResponse = json_decode($result);

        return new JsonResponse($arrResponse);

    }
}

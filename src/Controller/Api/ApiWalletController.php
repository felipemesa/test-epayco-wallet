<?php

namespace App\Controller\Api;

use nusoap_client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

class ApiWalletController extends AbstractController
{

    /**
     * @Rest\Post("/api/consult/wallet")
     */
    public function consultWallet(Request $request)
    {
        $client = new nusoap_client(getenv("BASE_URL_SOAP") . '/wallet/soap/consult?wsdl', 'wsdl');
        $client->setEndpoint(getenv("BASE_URL_SOAP") . '/wallet/soap/consult');

        $client->decode_utf8 = true;

        $objData = json_decode($request->getContent());
        $mobile = isset($objData->mobile) ? $objData->mobile : "";
        $identificationNumber = isset($objData->identificationNumber) ? $objData->identificationNumber : "";

        //Call soap
        $result = $client->call('consultWallet', ['identificationNumber' => $identificationNumber, 'mobile' => $mobile]);
        $arrResponse = json_decode($result);

        return new JsonResponse($arrResponse);
    }

    /**
     * @Rest\Post("/api/recharge/wallet")
     */
    public function rechargeWallet(Request $request)
    {

        $client = new nusoap_client(getenv("BASE_URL_SOAP") . '/wallet/soap/recharge?wsdl', 'wsdl');
        $client->setEndpoint(getenv("BASE_URL_SOAP") . '/wallet/soap/recharge');

        $client->decode_utf8 = true;


        $objData = json_decode($request->getContent());
        $mobile = isset($objData->mobile) ? $objData->mobile : "";
        $identificationNumber = isset($objData->identificationNumber) ? $objData->identificationNumber : "";
        $total = isset($objData->total) ? $objData->total : "";

        //Call soap
        $result = $client->call('rechargeWallet', ['identificationNumber' => $identificationNumber, 'mobile' => $mobile, 'total' => $total]);
        $arrResponse = json_decode($result);

        return new JsonResponse($arrResponse);
    }
}
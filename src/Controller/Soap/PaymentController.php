<?php

namespace App\Controller\Soap;

use App\Service\PaymentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaymentController extends AbstractController
{
    /**
     * @Route("/payment/soap/pay",name="pay")
     */
    public function pay(Request $request, PaymentService $paymentService)
    {

        $location = $this->getParameter('kernel.project_dir') . '/public/wsdl/payment.wsdl';
        $soapServer = new \SoapServer($location);

        $soapServer->setObject($paymentService);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml:text/xml; charset=UTF-8');

        ob_start();
        $soapServer->handle();

        $response->setContent(ob_get_clean());

        return $response;

    }

    /**
     * @Route("/payment/soap/confirm",name="paymentConfirm")
     */
    public function paymentConfirm(Request $request, PaymentService $paymentService)
    {

        $location = $this->getParameter('kernel.project_dir') . '/public/wsdl/payConfirm.wsdl';
        $soapServer = new \SoapServer($location);

        $soapServer->setObject($paymentService);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml:text/xml; charset=UTF-8');

        ob_start();
        $soapServer->handle();

        $response->setContent(ob_get_clean());

        return $response;

    }
}

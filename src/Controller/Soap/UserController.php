<?php

namespace App\Controller\Soap;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/create/user/soap", name="createUserSoap")
     * @param Request $request
     * @param UserService $userService
     * @return Response
     */
    public function index(Request $request, UserService $userService)
    {
        $location = $this->getParameter('kernel.project_dir') . '/public/wsdl/user.wsdl';
        $soapServer = new \SoapServer($location);

        $soapServer->setObject($userService);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml:text/xml; charset=UTF-8');

        ob_start();
        $soapServer->handle();

        $response->setContent(ob_get_clean());

        return $response;
    }
}

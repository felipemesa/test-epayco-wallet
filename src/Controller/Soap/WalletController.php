<?php

namespace App\Controller\Soap;

use App\Service\WalletService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WalletController extends AbstractController
{

    /**
     * @Route("/wallet/soap/consult",name="consultWallet")
     */
    public function consultWallet(Request $request, WalletService $walletService)
    {

        $location = $this->getParameter('kernel.project_dir') . '/public/wsdl/consultWallet.wsdl';
        $soapServer = new \SoapServer($location);
        $soapServer->setObject($walletService);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml:text/xml; charset=UTF-8');

        ob_start();
        $soapServer->handle();

        $response->setContent(ob_get_clean());

        return $response;

    }

    /**
     * @Route("/wallet/soap/recharge",name="rechargeWallet")
     */
    public function rechargeWallet(Request $request, WalletService $walletService)
    {

        $location = $this->getParameter('kernel.project_dir') . '/public/wsdl/wallet.wsdl';
        $soapServer = new \SoapServer($location);

        $soapServer->setObject($walletService);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml:text/xml; charset=UTF-8');

        ob_start();
        $soapServer->handle();

        $response->setContent(ob_get_clean());

        return $response;

    }
}

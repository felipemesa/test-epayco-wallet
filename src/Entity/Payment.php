<?php

namespace App\Entity;

use App\Repository\PaymentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaymentRepository::class)
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $token;

    /**
     * @ORM\Column(type="integer")
     */
    private $walletId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wallet", inversedBy="paymentRel")
     * @ORM\JoinColumn(name="wallet_id", referencedColumnName="id")
     */
    private $walletRel;

    /**
     * @ORM\Column(type="boolean")
     */
    private $confirmed = false;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getWalletId()
    {
        return $this->walletId;
    }

    /**
     * @param mixed $walletId
     */
    public function setWalletId($walletId): void
    {
        $this->walletId = $walletId;
    }

    /**
     * @return mixed
     */
    public function getWalletRel()
    {
        return $this->walletRel;
    }

    /**
     * @param mixed $walletRel
     */
    public function setWalletRel($walletRel): void
    {
        $this->walletRel = $walletRel;
    }

    /**
     * @return mixed
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param mixed $confirmed
     */
    public function setConfirmed($confirmed): void
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total): void
    {
        $this->total = $total;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WalletRepository::class)
 */
class Wallet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @ORM\Column(type="float")
     */
    private $balance = 0;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="walletRel")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userRel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Payment", mappedBy="walletRel")
     */
    private $paymentRel;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance): void
    {
        $this->balance = $balance;
    }

    /**
     * @return mixed
     */
    public function getUserRel()
    {
        return $this->userRel;
    }

    /**
     * @param mixed $userRel
     */
    public function setUserRel($userRel): void
    {
        $this->userRel = $userRel;
    }

    /**
     * @return mixed
     */
    public function getPaymentRel()
    {
        return $this->paymentRel;
    }

    /**
     * @param mixed $paymentRel
     */
    public function setPaymentRel($paymentRel): void
    {
        $this->paymentRel = $paymentRel;
    }
}

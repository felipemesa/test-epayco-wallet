<?php


namespace App\Service;

use App\Entity\Payment;
use App\Entity\User;
use App\Repository\PaymentRepository;
use App\Repository\UserRepository;
use App\Repository\WalletRepository;
use App\Validation\Validate;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class PaymentService
{
    /** @var EntityManagerInterface */
    var $em;

    /** @var UserRepository */
    var $userRepository;

    /** @var WalletRepository */
    var $walletRepository;

    /** @var PaymentRepository */
    var $paymentRepository;

    /** @var MailerInterface */
    var $mailer;

    /**
     * PaymentService constructor.
     * @param UserRepository $userRepository
     * @param WalletRepository $walletRepository
     * @param PaymentRepository $paymentRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UserRepository $userRepository, WalletRepository $walletRepository, PaymentRepository $paymentRepository, EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $this->em = $entityManager;
        $this->userRepository = $userRepository;
        $this->walletRepository = $walletRepository;
        $this->paymentRepository = $paymentRepository;
        $this->mailer = $mailer;
    }

    public function pay($identificationNumber, $total, $email)
    {
        //Validar los campos vacios
        $validate = new Validate();

        $videntificationNumber = $validate->validateEmpty($identificationNumber);
        if (!$videntificationNumber) {
            $validate->setError(415, "El campo identificationNumber es obligatorio");
        }

        $vemail = $validate->validateEmpty($email);
        if (!$vemail) {
            $validate->setError(415, "El campo email es obligatorio");
        }

        $vtotal = $validate->validateEmpty($total);
        if (!$vtotal && $vtotal > 0) {
            $validate->setError(415, "El campo total es obligatorio");
        }

        if ($validate->totalErrors > 0) {
            $arrResponse = [
                'success' => false,
                'code' => 415,
                'message' => "Campos inválidos, por favor corregir los errores.",
                "data" => [
                    'totalErrors' => $validate->totalErrors,
                    'errors' => $validate->errorMessage,
                ]
            ];
            return json_encode($arrResponse);
        }

        try {

            /** @var  $arUser User */
            $arUser = $this->userRepository->validateUserExists($identificationNumber, $email);

            if ($arUser) {
                $arWallet = $this->walletRepository->findOneBy(["userId" => $arUser->getId()]);
                if (($arWallet->getBalance() - $total) < 0) {
                    $arrResponse = [
                        'success' => false,
                        'code' => 415,
                        'message' => "Saldo insuficiente.",
                        "data" => ["balance" => $arWallet->getBalance()]
                    ];
                    return json_encode($arrResponse);
                }

                $arPago = new Payment();
                $token = $this->generateToken();
                $arPago->setWalletRel($arWallet);
                $arPago->setTotal($total);
                $arPago->setConfirmed(false);
                $arPago->setToken($token);
                $this->em->persist($arPago);
                $this->em->flush();

                $arrResponse = [
                    'success' => true,
                    'code' => 415,
                    'message' => "Hemos enviado un token a su correo.",
                    "data" => [
                        "name" => $arUser->getName(),
                        "identificationNumber" => $arUser->getIdentificationNumber(),
                        "mobile" => $arUser->getMobile(),
                        "email" => $arUser->getEmail(),
                    ]];

                $this->sendEmail($email, $token);

            } else {
                $arrResponse = [
                    'success' => false,
                    'code' => 416,
                    'message' => "El número de identificación no existe.",
                ];
            }
        } catch (\Exception $e) {
            $arrResponse = [
                'success' => false,
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                "data" => $e
            ];
        }

        return json_encode($arrResponse);
    }

    public function paymentConfirm($identificationNumber, $token)
    {

        //Validar los campos vacios
        $validate = new Validate();

        $videntificationNumber = $validate->validateEmpty($identificationNumber);
        if (!$videntificationNumber) {
            $validate->setError(415, "El campo identificationNumber es obligatorio");
        }

        $vtoken = $validate->validateEmpty($token);
        if (!$vtoken) {
            $validate->setError(415, "El campo token es obligatorio");
        }

        if ($validate->totalErrors > 0) {
            $arrResponse = [
                'success' => false,
                'code' => 415,
                'message' => "campos inválidos",
                "data" => [
                    'totalErrors' => $validate->totalErrors,
                    'errors' => $validate->errorMessage
                ]
            ];
            return json_encode($arrResponse);
        }
        try {
            /** @var  $arUser User */
            $arUser = $this->userRepository->validateUserExists($identificationNumber);
            if ($arUser) {

                /** @var  $arPayment Payment */
                $arPayment = $this->paymentRepository->findOneBy(['token' => $token]);

                if ($arPayment) {
                    $arWallet = $this->walletRepository->findOneBy(["userId" => $arUser->getId()]);

                    $total = $arPayment->getTotal();
                    if (($arWallet->getBalance() - $total) < 0) {
                        $arrResponse = [
                            'success' => false,
                            'code' => 415,
                            'message' => "Saldo insuficiente",
                            "data" => ["balance" => $arUser->getWalletRel()->getBalance()]
                        ];
                        return json_encode($arrResponse);
                    }


                    if (!$arPayment->getConfirmed()) {

                        $arPayment->setConfirmed(true);
                        $balance = $arUser->getWalletRel()->getBalance();
                        $balance -= $total;
                        $arWallet->setBalance($balance);
                        $this->em->persist($arWallet);
                        $this->em->flush();

                        $arrResponse = [
                            'success' => true,
                            'code' => 415,
                            'message' => "Pago procesado exitosamente",
                            "data" => [
                                "name" => $arUser->getName(),
                                "identificationNumber" => $arUser->getIdentificationNumber(),
                                "mobile" => $arUser->getMobile(),
                                "email" => $arUser->getEmail(),
                                "balance" => $balance
                            ]];
                    } else {
                        $arrResponse = [
                            'success' => false,
                            'code' => 417,
                            'message' => "El pago ya fue confirmado y no se puede volver a procesar.",
                        ];
                    }
                } else {
                    $arrResponse = [
                        'success' => false,
                        'code' => 417,
                        'message' => "El pago no puede ser confirmado, el token no es válido",
                    ];
                }

            } else {
                $arrResponse = [
                    'success' => false,
                    'code' => 417,
                    'message' => "El pago no puede ser confirmado.",
                ];
            }
        } catch (\Exception $exception) {
            $arrResponse = [
                'success' => false,
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
                "data" => $exception
            ];
        }

        return json_encode($arrResponse);
    }

    private function generateToken()
    {
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '1234567890';
        $randomString = '';
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $letters[rand(0, strlen($letters) - 1)];
        }
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $digits[rand(0, strlen($digits) - 1)];
        }

        return $randomString;
    }

    private function sendEmail($email, $token)
    {
        $email = (new Email())
            ->from("info@prontofixgas.com")
            ->to($email)
            ->subject('Se ha generado un token para su pago.')
            ->text("Ingrese el siguiente token para confirmar su pago: {$token}")
            ->html("<p>Ingrese el siguiente token para confirmar su pago: {$token}</p>");

        $this->mailer->send($email);
    }

}
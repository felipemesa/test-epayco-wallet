<?php


namespace App\Service;

use App\Entity\User;
use App\Entity\Wallet;
use App\Repository\UserRepository;
use App\Validation\Validate;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    /** @var EntityManagerInterface */
    var $em;

    /** @var UserRepository */
    var $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function createUser($name, $identificationNumber, $mobile, $email)
    {
        try {
            //Validar los campos vacios
            $validate = new Validate();
            $vname = $validate->validateEmpty($name);
            if (!$vname) {
                $validate->setError(415, "El campo name es requerido");
            }

            $videntificationNumber = $validate->validateEmpty($identificationNumber);
            if (!$videntificationNumber) {
                $validate->setError(415, "El campo identificacionNumber es requerido");
            }

            $vmobile = $validate->validateEmpty($mobile);
            if (!$vmobile) {
                $validate->setError(415, "El campo mobile es requerido");
            }

            $vemail = $validate->validateEmpty($email);
            if (!$vemail) {
                $validate->setError(415, "El campo email es requerido");
            }

            if ($validate->totalErrors > 0) {
                $arrResponse = [
                    'success' => false,
                    'code' => 415,
                    'message' => "Error en los campos por favor corregir.",
                    "data" => [
                        'totalErrors' => $validate->totalErrors,
                        'errors' => $validate->errorMessage
                    ]
                ];
                return json_encode($arrResponse);
            }

            //Si todos los campos son validos se realiza logica de crear usuario en la base de datos

            //Validar que el usuario no se encuentre registrado por email o por numero de identificación
            if ($this->userRepository->validateUserExists($identificationNumber, $email)) {
                $arrResponse = [
                    'success' => false,
                    'code' => 416,
                    'message' => "El email o numero de identificación ya existe.",
                    "data" => []
                ];
                return json_encode($arrResponse);
            }

            //Si el usuario no se ha registrado se procede a realizar la creación del cliente
            $arUser = new User();
            $arUser->setName($name);
            $arUser->setIdentificationNumber($identificationNumber);
            $arUser->setEmail($email);
            $arUser->setMobile($mobile);
            $this->em->persist($arUser);

            $arWallet = new Wallet();
            $arWallet->setBalance(0);
            $arWallet->setUserRel($arUser);
            $this->em->persist($arWallet);

            $this->em->flush();

            $arrResponse = [
                'success' => true,
                'code' => 415,
                'message' => "El usuario fue registrado exitosamente.",
                "data" => [
                    "name" => $arUser->getName(),
                    "identificationNumber" => $arUser->getIdentificationNumber(),
                    "mobile" => $arUser->getMobile(),
                    "email" => $arUser->getEmail(),
                ]
            ];

            return json_encode($arrResponse);

        } catch (\Exception $exception) {
            $arrResponse = [
                'success' => false,
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
            return json_encode($arrResponse);
        }
    }

}
<?php


namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\WalletRepository;
use App\Validation\Validate;
use Doctrine\ORM\EntityManagerInterface;

class WalletService
{
    /** @var EntityManagerInterface */
    var $em;

    /** @var UserRepository */
    var $userRepository;

    /** @var WalletRepository */
    var $walletRepository;

    /**
     * WalletService constructor.
     * @param UserRepository $userRepository
     * @param WalletRepository $walletRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UserRepository $userRepository, WalletRepository $walletRepository, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->userRepository = $userRepository;
        $this->walletRepository = $walletRepository;
    }

    public function consultWallet($identificationNumber, $mobile)
    {
        //Validar los campos vacios
        $validate = new Validate();

        $videntificationNumber = $validate->validateEmpty($identificationNumber);
        if (!$videntificationNumber) {
            $validate->setError(415, "El campo identificationNumber es obligatorio");
        }

        $vmobile = $validate->validateEmpty($mobile);
        if (!$vmobile) {
            $validate->setError(415, "El campo mobile es obligatorio");
        }

        if ($validate->totalErrors > 0) {
            $arrResponse = [
                'success' => false,
                'code' => 415,
                'message' => "invalid fields",
                "data" => [
                    'totalErrors' => $validate->totalErrors,
                    'errors' => $validate->errorMessage
                ]
            ];
            return json_encode($arrResponse);
        }

        try {
            //Validar que el usuario no se encuentre registrado por email o por numero de identificación
            /** @var  $arUser User */
            $arUser = $this->userRepository->validateUserExists($identificationNumber, "", $mobile);
            if (!$arUser) {
                $arrResponse = [
                    'success' => false,
                    'code' => 416,
                    'message' => "El npumero de celular no corresponde a el usuario",
                    "data" => []
                ];
                return json_encode($arrResponse);
            }

            $balance = $arUser->getWalletRel()->getBalance();
            $arrResponse = [
                'success' => true,
                'code' => 415,
                'message' => "Saldo consultado exitosamente",
                "data" => [
                    "name" => $arUser->getName(),
                    "identificationNumber" => $arUser->getIdentificationNumber(),
                    "mobile" => $arUser->getMobile(),
                    "email" => $arUser->getEmail(),
                    "balance" => $balance
                ]
            ];

        } catch (\Exception $exception) {
            $arrResponse['success'] = false;
            $arrResponse['code'] = $exception->getCode();
            $arrResponse['message'] = $exception->getMessage();
        }

        return json_encode($arrResponse);

    }

    public function rechargeWallet($identificationNumber, $mobile, $total)
    {
        //Validar los campos vacios
        $validate = new Validate();

        $videntificationNumber = $validate->validateEmpty($identificationNumber);
        if (!$videntificationNumber) {
            $validate->setError(415, "El campo identificationNumber es obligatorio");
        }

        $vmobile = $validate->validateEmpty($mobile);
        if (!$vmobile) {
            $validate->setError(415, "El campo mobile es obligatorio");
        }

        $vtotal = $validate->validateEmpty($total);
        if (!$vtotal) {
            $validate->setError(415, "El campo total es obligatorio");
        }
        if ($validate->totalErrors > 0) {
            $arrResponse = [
                'success' => false,
                'code' => 415,
                'message' => "invalid fields",
                "data" => [
                    'totalErrors' => $validate->totalErrors,
                    'errors' => $validate->errorMessage
                ]
            ];
            return json_encode($arrResponse);
        }

        try {

            //Validar que el usuario no se encuentre registrado por email o por numero de identificación
            if (!$this->userRepository->validateUserExists($identificationNumber, "", $mobile)) {
                $arrResponse = [
                    'success' => false,
                    'code' => 416,
                    'message' => "El celular no corresponde a el documento",
                    "data" => []
                ];
                return json_encode($arrResponse);
            }


            /** @var  $arUser User */
            $arUser = $this->userRepository->findOneBy(['identificationNumber' => $identificationNumber]);

            $arWallet = $this->walletRepository->findOneBy(["userId" => $arUser->getId()]);
            $balance = $arWallet->getBalance();
            $newBalance = $balance + $total;
            $arWallet->setBalance($newBalance);
            $this->em->persist($arWallet);
            $this->em->flush();

            $arrResponse = [
                'success' => true,
                'code' => 415,
                'message' => 'Saldo recargado exitosamente, su nuevo saldo es: ' . $newBalance,
                "data" => [
                    "name" => $arUser->getName(),
                    "identificationNumber" => $arUser->getIdentificationNumber(),
                    "mobile" => $arUser->getMobile(),
                    "email" => $arUser->getEmail(),
                    "balance" => $newBalance
                ]
            ];

        } catch (\Exception $exception) {
            $arrResponse['success'] = false;
            $arrResponse['code'] = $exception->getCode();
            $arrResponse['message'] = $exception->getMessage();
        }

        return json_encode($arrResponse);
    }

}
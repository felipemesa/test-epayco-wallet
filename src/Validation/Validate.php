<?php namespace App\Validation;


class Validate
{

    public $errorMessage;
    public $totalErrors;
    public $validator;

    public function __construct()
    {
        $this->errorMessage = array();
        $this->totalerrors = 0;
    }

    public function validateEmpty($value)
    {

        if (strlen($value) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function setError($code, $message)
    {
        $this->errorMessage[] = array('code' => $code, 'message' => $message);
        $this->totalErrors++;
    }
}